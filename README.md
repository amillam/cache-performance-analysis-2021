**How to run *Cache Performance Analysis.asm*:**

1. Ensure the computer has [QtSpim](https://sourceforge.net/projects/spimsimulator/) installed. If it does not, install QtSpim from the given link.

2. Open QtSpim and navigate to *File > Load File*. Navigate to *Cache Performance Analysis.asm* and open the file.

3. Click "Run/Continue", located in the *Simulator* tab or on the toolbar below it.

4. Record the output from Console. If the console is not visible, go to the *Window* tab and ensure "Console" is checked.